### Step by step instructions

Required version of node is 10+

1. `npm i` in the root folder of this project.
2. `npm run build` - builds the required assets.
3. `npm run dev` - launches built-in web-server and file watchers.

#### Folder structure explanation
Source files are located in: "src" directory.
Compiled assets located in: "dist" directory.
If you plan to change something, then make the changes in "src" folder, then re-build them using commands above.

#### HTML pages
- main page's URL: http://localhost:3000 > `src/pages/main.html`
- movie page's URL: http://localhost:3000/movie > `src/pages/movie.html`
- search page's URL: http://localhost:3000/search > `src/pages/search.html`
- not found page's URL: http://localhost:3000/not-found > `src/pages/not-found.html`

#### HTML partials
Are located in the following directory: `src/pages/partials`
