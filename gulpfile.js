'use strict'

const gulp = require('gulp')
const $ = require('gulp-load-plugins')()
const del = require('del')
const browserSync = require('browser-sync').create()

function clean() {
  return del(['./dist'])
}

function browsSync(done) {
  browserSync.init({
    server: {
      baseDir: ['./', './dist/pages'],
      index: 'main.html',
      serveStaticOptions: {
        extensions: ['html']
      }
    }
  })
  done()
}

function styles() {
  return gulp
    .src('./src/assets/scss/app.scss')
    .pipe($.sass())
    .pipe($.autoprefixer())
    .pipe($.cleanCss())
    .pipe(gulp.dest('dist/css'))
    .pipe(browserSync.stream())
}

function scripts() {
  return gulp
    .src(['./src/assets/js/custom.js'])
    .pipe($.concat('app.js'))
    .pipe(
      $.babel({
        presets: ['@babel/preset-env']
      })
    )
    .pipe(
      $.uglify().on('error', function(e) {
        console.log(e)
      })
    )
    .pipe($.rename({ suffix: '.min' }))
    .pipe(gulp.dest('dist/js'))
}

function images() {
  return gulp
    .src('./src/assets/images/**/*')
    .pipe(
      $.imagemin([
        $.imagemin.gifsicle({ interlaced: true }),
        $.imagemin.jpegtran({ progressive: true }),
        $.imagemin.optipng({ optimizationLevel: 5 }),
        $.imagemin.svgo({
          plugins: [
            {
              removeViewBox: false,
              collapseGroups: true
            }
          ]
        })
      ])
    )
    .pipe(gulp.dest('./dist/images'))
}

function partials() {
  return gulp
    .src('./src/pages/*.html')
    .pipe($.fileInclude())
    .pipe(gulp.dest('./dist/pages'))
}

function fonts() {
  return gulp
    .src('./src/assets/fonts/*.{eot,otf,svg,ttf,woff}')
    .pipe($.copy('./dist/fonts/', { prefix: 3 }))
}

function watchFiles() {
  gulp.watch('./src/assets/scss/*.scss', styles)
  gulp
    .watch(['./src/assets/js/*.js', '!./src/assets/js/app.min.js'], scripts)
    .on('change', browserSync.reload)
  gulp
    .watch('./src/assets/images/**/*', images)
    .on('change', browserSync.reload)
  gulp.watch('./src/pages/**/*.html').on('change', gulp.series(partials, browserSync.reload))
}

/* complex tasks */
const watch = gulp.parallel(watchFiles, browsSync)
const build = gulp.series(clean, styles, scripts, images, fonts, partials)

/* exposing tasks */
exports.watch = watch
exports.styles = styles
exports.scripts = scripts
exports.build = build
exports.images = images
exports.fonts = fonts
exports.partials = partials
exports.clean = clean
